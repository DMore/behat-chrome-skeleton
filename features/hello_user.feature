Feature: Hello world
  In order to be nice
  As a polite PHP script
  I need to say hello to my user

  Scenario: It says hello world
    Given I am on "/index.php?name=John"
    Then I should see "Hello, John"
