Feature: Hello world
  In order to be nice
  As a polite html page
  I need to say hello

Scenario: It says hello world
  Given I am on "/index.html"
  Then I should see "Hello, world"
